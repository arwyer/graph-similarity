all: vertex_rank

vertex_rank: graph_sim.cpp graph_sim.h
	g++ -g -o graph_sim graph_sim.cpp

clean:
	/bin/rm graph_sim
