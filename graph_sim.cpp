//--------------------------------------------------------------
// Vertex Ranking/Network Augmentation
//
// Charles Phillips
// Department of Electrical Engineering and Computer Science
// University of Tennessee
//--------------------------------------------------------------

#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>
#include <algorithm>
#include <set>
#include <sys/stat.h>
#include "graph_sim.h"
#include <map>
#include <ctime>
#include <cstdlib>
#include <sstream>

using namespace std;

string VERSION = "vertex_rank v1.0";
int METHOD = 0;

void usage()
{
  cerr << "usage: vertex_rank <phi> <method> < <graphs to examine> <number of pairs to output> \n\n";
  cerr << "Inputs a matrix and a set of vertices and outputs the candidate score for each vertex\n";
  cerr << "Phi is the weight (0 to 1) given to direct vs. indirect similarity.\n";
  cerr << "Method is one of 0: direct\n";
  cerr << "                 1: indirect\n";
  cerr << "                 2: indirect with decaying phi, internal known weights included\n";
  cerr << "                 3: indirect with decaying phi, internal known weights not included\n";
  cerr << "Similarity matrix is a matrix containing the weights of all edges, for instance a\n";
  cerr << "correlation matrix. Both row and column labels must be present and should contain all vertices in the graph.\n";
  exit(1);
}

//========================================
// Returns whether or not a file exists
//========================================
bool fileExists(const char* filename)
{
  struct stat buffer;
  if (stat(filename, &buffer))
    return false;
  return true;
}

//Find common vertices between 2 sets of labels
void findCommonVertices(vector<string>& labels1, vector<string>& labels2, vector<string>& common)
{
	for(vector<string>::iterator it = labels1.begin(); it != labels1.end(); ++it){
		if(find(labels2.begin(),labels2.end(),*it) != labels2.end()){
			common.push_back(*it);
		}
	}

}

//check that all known subgraph labels exist, and set up known vector with indices
void checkVertices(vector <string>& vertex_labels, vector <string>& known_labels, vector <int>& known)
{
  int i, j;
  bool found;

  for (i=0; i<known_labels.size(); i++)
  {
    found = false;
    for (j=0; j<vertex_labels.size(); j++)
    {
      if (known_labels[i] == vertex_labels[j])
      {
        found = true;
        known.push_back(j);
        break;
      }
    }
    if (!found)
    {
      cerr << "Error: there is no vertex label '" << known_labels[i] << "' in the input matrix\n";
      exit(1);
    }
  }

}

//Vertex ranking only taking into account neighbors
void directRank(vector <vector <double> >& matrix, vector <string>& vertex_labels,
                vector <string>& known_labels, vector <int>& known, vector <Score>& scores)
{
  int i, j;
  Score s;

  for (i=0; i<vertex_labels.size(); i++)  //calculate average weight of each vertex's connections to the subgraph
  {
    s.label = vertex_labels[i];
    s.score = 0;
    for (j=0; j<known.size(); j++)
      s.score += matrix[i][known[j]];
    s.score = s.score / (double)known.size();
    scores.push_back(s);
  }
}

//Finds largest difference in scores
double largestDiff(vector <Score>& scores, vector <Score>& newScores)
{
  int i;
  double diff, largestDiff = 0.0;

  for (i=0; i<scores.size(); i++)
  {
    diff = fabs(scores[i].score - newScores[i].score);
    if (diff > largestDiff) largestDiff = diff;
  }
  return largestDiff;
}

//Produce augmentation scores based on indirect rank
void indirectRank(vector <vector <double> >& matrix, vector <string>& vertex_labels, vector <string>& known_labels, vector <int>& known, vector <Score>& scores, double phi)
{
  int i, j, k;
  Score s;
  vector <bool> isKnown;
  vector <Score> newScores;
  double sumOfProducts;

  for (i=0; i<vertex_labels.size(); i++)
    isKnown.push_back(false);
  for (i=0; i<known.size(); i++)
    isKnown[known[i]] = true;

  for (i=0; i<vertex_labels.size(); i++)   //set scores to zero
  {
    s.label = vertex_labels[i];
    s.score = 0;
    scores.push_back(s);
  }
  newScores = scores;

  if (METHOD == 1)
  {
    for (i=0; i<known.size(); i++)          //set known scores to one
      scores[known[i]].score = 1.0;

    k = 0;
    while (true)  //keep recalcualting until convergence
    {
      for (i=0; i<scores.size(); i++)
      {
        sumOfProducts = 0.0;
        for (j=0; j<scores.size(); j++)
          sumOfProducts += matrix[i][j] * scores[j].score;

        newScores[i].score = phi *  sumOfProducts / scores.size();
        if (isKnown[i]) newScores[i].score += 1.0;
      }

      //test for the largest change from the last calculation
      double change = largestDiff(scores, newScores);
//      cout << change << endl;
      scores = newScores;

//      for (int m=0; m<scores.size(); m++)
//        cout << vertex_labels[m] << "\t" << scores[m].score << endl;
//      cout << endl;
	  
	  //cout<<"change: " <<change <<endl;
      if (change < .00000001) break;
      k++;
    }
  }

  if (METHOD == 2 || METHOD == 3)        //use method 2 or 3
  {
    for (i=0; i<scores.size(); i++)    //start with average of direct weights
    {
      scores[i].score = 0;
      for (j=0; j<known.size(); j++)
        scores[i].score += matrix[i][known[j]];
      scores[i].score = scores[i].score / (double)known.size();
    }

    k = 0;
    double newphi = phi;
    while (true)       //recalculate until convergence
    {
      for (i=0; i<scores.size(); i++)
      {
        sumOfProducts = 0.0;
        for (j=0; j<scores.size(); j++)
        {
          if (METHOD == 3 && isKnown[j]) continue;  //skip known vertices

          sumOfProducts += matrix[i][j] + scores[j].score;
        }
        double average = sumOfProducts / (scores.size() * 2.0);

        newScores[i].score = (scores[i].score + newphi*average) / (1.0 + newphi);
      }

      newphi = newphi * phi;

      //test for the largest change from the last calculation
      double change = largestDiff(scores, newScores);
//      cout << change << endl;
      scores = newScores;

//      for (int m=0; m<scores.size(); m++)
//        cout << vertex_labels[m] << "\t" << scores[m].score << endl;
//      cout << endl;
     
	  //cout<<"change: " <<change <<endl;
	  if (change < .00000001) break;
      k++;
    }
  }

   //cerr << "converged after " << k << " iterations\n";
}

//Calculates similarity of two graphs based on the augment algorithm
//Finds common vertices between the two graphs
//Runs augment for those vertices on each graph
//Calculates difference in scores for those vertices
//Determines similarity based on the score difference
double graphSim(vector<vector<double> > &matrix1, vector<vector<double> > &matrix2, vector<string> &labels1, vector<string> &labels2, int method, double phi)
{
	vector<vector<Score> > scores;
	vector<vector<Score> > scores2;
	vector<string> commonVerts;
	vector<string> commonVerts2;
	findCommonVertices(labels1,labels2, commonVerts);
	if(commonVerts.size() == 0){
		return -1;
	}
	commonVerts2 = commonVerts;


	vector<vector<int> > knownVec;
	knownVec.resize(commonVerts.size());
	for(int i = 0; i < knownVec.size(); i++){
		vector<string> temp;
		temp.push_back(commonVerts[i]);
		checkVertices(labels1,temp,knownVec[i]);
	}

	//Perform rankings for each common vertex
	scores.resize(commonVerts.size());
	scores2.resize(commonVerts.size());
	if(method == 0){
		for(int i = 0; i < commonVerts.size(); i++){
			directRank(matrix1,labels1,commonVerts,knownVec[i],scores[i]);
			directRank(matrix2,labels2,commonVerts,knownVec[i],scores2[i]);
		}
	}
	else{
		for(int i = 0; i < commonVerts.size(); i++){
			indirectRank(matrix1,labels1,commonVerts,knownVec[i],scores[i],phi);
			indirectRank(matrix2,labels2,commonVerts,knownVec[i],scores2[i],phi);
		}
	}
	
	//Find difference in augmentation for each of the common vertices
	//use this to score similarity
	double diffSumAvg = 0;
	for(int i = 0; i < scores.size(); i++){
		double diffsum = 0;
		for(int j = 0; j < scores[i].size(); j++){
			for(int k = 0; k < scores2[i].size(); k++){
				if(scores[i][j].label == scores2[i][k].label){
					double diff = abs(scores[i][j].score - scores[i][k].score);
					diffsum += diff;
				}
			}
		}
		diffsum = diffsum/commonVerts.size();
		diffSumAvg += diffsum;
	}
	return diffSumAvg;
}


//==================================================
// Removes any trailing whitespace from a string.
//==================================================
void chompBack(string& str)
{
  int i = str.length() - 1;

  while (i>=0 && (str[i] == '\n' || str[i] == '\t' || str[i] == '\r' || str[i] == ' ') )
    i--;

  str = str.substr(0, i+1);
}

//=======================================================================
// splits a null terminated character array s into a vector of strings
// c is the character to split on
//=======================================================================
void split(vector<string>& v, const char* s, char c)
{
  v.clear();
  while (true)
  {
    //cout<<"split\n";
	const char* begin = s;

    while (*s != c && *s) { ++s; }

    v.push_back(string(begin, s));

    if (!*s) break;

    if (!*++s) break;
  }

  if (*--s == c)                            //if last character is c, append another empty string
    v.push_back("");
}

//================================================================
// Check the command-line arguments and set up the global flags
//================================================================
void parseArgs(int argc, char** argv)
{
  if (argc != 5) usage();

  METHOD = atoi(argv[2]);
  if (METHOD < 0 || METHOD > 3)
  {
    cerr << "METHOD (fourth argument) must be 0, 1, 2 or 3\n";
    exit(0);
  }

  double phi = atof(argv[1]);
  if (phi < 0.0 || phi > 1.0)
  {
    cerr << "phi must be between 0 and 1\n";
    exit(0);
  }
  int numPairs = atoi(argv[4]);
  if(numPairs < 0){
	  cerr<<"numPairs must be greater than 0\n";
	exit(0);
  }
}

//Function used for producing random strings (useful for test labels)
string gen_random( const int len) {
	char* s = (char*)malloc(sizeof(char)*(len+1));
	static const char alphanum[] =
		"0123456789"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";

	for (int i = 0; i < len; ++i) {
		s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
	}

	s[len] = 0;
	string str(s);
	delete s;
	return str;
}

//Fill a vector with random labels
void randLabels(vector<string>& labels, int numLab)
{
	int len = 8;
	for(int i = 0; i < numLab; i++){
		labels.push_back(gen_random(len));
	}
}

//Generate the matrix for an empty graph
void emptyGraph(vector<vector<double> >&matrix, int dim)
{
	for(int i = 0; i < dim; i++){
		for(int j = 0; j < dim; j++){
			matrix[i][j] = 0;
		}
	}
}

//Generate an almost clique graph
void randAlmostClique(vector<vector<double> >& matrix, int dim)
{
	for(int i = 0; i < dim; i++){
		for(int j = 0; j < dim; j++){
			if(i != j){
				matrix[i][j] = 1.0;
			}
		}
	}
	int n = rand()%(dim-1) + 1;
	matrix[0][n] = 0.0;
	matrix[n][0] = 0.0;
}

//Generate a random graph
void randBinMatrix(vector<vector<double> >& matrix, int dim)
{
	for(int i = 0; i < dim; i++){
		for(int j = (i+1); j < dim; j++){
			int n = rand()%10;
			if(n){
				matrix[i][j] = 1;
				matrix[j][i] = 1;
			}
			else{
				matrix[i][j] = 0;
				matrix[j][i] = 0;
			}
		}
	}

}


//Generate an isomorphism for a graph
void matIso(vector<vector<double> >& copy, vector<vector<double> >& orig)
{
	copy = orig;
}

//Generate the complement of a graph
void matBinComplement(vector<vector<double> >& comp, vector<vector<double> >& orig)
{
	for(int i = 0; i < orig.size(); i++){
		for(int j = 0; j < orig[i].size(); j++){
			if(i != j){
				if(orig[i][j] == 1.0){
					comp[i][j] = 0;
				}
				else{
					comp[i][j] = 1;
				}
			}
		}
	}
}

//Do a "real" complement of a graph
//e.g. G'[i][j] = 1 - G[i][j]
void matRealComplement(vector<vector<double> >& comp, vector<vector<double> >& orig)
{
	for(int i = 0; i < orig.size(); i++){
		for(int j = 0; j < orig[i].size(); j++){
			comp[i][j] = 1.0 - orig[i][j];
		}
	}
}

void randMatrix(vector<vector<double> >& matrix, int dim)
{
	for(int i = 0; i < dim; i++){
		vector<double> correlations;
		for(int j = 0; j < dim; j++){
			correlations.push_back(((double)rand()/(double)RAND_MAX));	
		}
		matrix.push_back(correlations);
	}
}

//read the correlation matrix from standard input
//Changing this to take a text file
//read matrix from each text file
void readMatrix(vector <vector <double> >& matrix, vector <string>& vertex_labels,string& graphFileName)
{
  int i, j;
  string str;
  vector <string> fields;
  vector <double> v;
  ifstream graphFile(graphFileName.c_str());
  if(!graphFile.good()){
	  cerr<<"Error: Unable to open graph " <<graphFileName <<" specified in graphs file\n";
  }
  //Replace the split/chompback with a sstream
  stringstream ss;
  getline(graphFile, str);         //read the first row, vertex labels
  //chompBack(str);
  //split(vertex_labels, str.c_str(), '\t');
  //vertex_labels.erase(vertex_labels.begin());  //remove first element
  ss.str(str);

  string tmp;
  while(ss >> tmp){
	  vertex_labels.push_back(tmp);
  }
	for (i=0; i<vertex_labels.size(); i++)  //initialize matrix
    matrix.push_back(v);

  i = 0;
  ss.clear();
  double tmpd;
  while (getline(graphFile, str))                 //read each row
  {
	ss.str(str);
	while(ss >> tmpd){
        matrix[i].push_back(tmpd);
	}
    //split(fields, str.c_str(), '\t');
    //for (j=1; j<fields.size(); j++)
      //matrix[i].push_back(fabs(atof(fields[j].c_str())));
    i++;
	ss.clear();
  }
}

void printMatrix(vector <vector <double> >& matrix, vector <string>& vertex_labels)
{
  int i, j;

  for (i=0; i<vertex_labels.size(); i++)
  {
    cout << vertex_labels[i];
    for (j=0; j<vertex_labels.size(); j++)
      cout << "\t" << matrix[i][j];
    cout << endl;
  }
}


//read the vertices in the known subgraph from a file
void readKnown(char* filename, vector <string>& known)
{
  ifstream f;
  string str;

  f.open(filename);
  if (!f) { cerr << "Error opening input file '" << filename << "'\n"; exit(1); }

  while (f >> str)
    known.push_back(str);
	//cout <<"known\n";
  f.close();

//  cerr << "using known cluster:\n";
//  for (int i=0; i<known.size(); i++)
//    cerr << known[i] << "\t";
//  cerr << endl;
}


int main(int argc, char** argv)
{
  int num_gloms;
  double maxoverlap;
  vector <vector <double> > matrix;  //correlation matrix
  vector <string> vertex_labels;
  vector <string> known_labels;
  vector <int> known;
  Score s;
  vector< vector <Score> > scores;
  vector< vector <Score> > scores2;
  vector< vector<string> > all_known_labels;


  vector<vector <vector <double> > > matrices;
  vector<vector <string> > all_vertex_labels;
  vector<vector <int> > knownVec;
  vector<vector<double> > simScores;

  cerr << VERSION << endl;;

  parseArgs(argc, argv);       //parse the arguments and set globals
  double phi = atof(argv[1]);

  string graphsFileName(argv[3]);
  ifstream graphsFile(graphsFileName.c_str(), ifstream::in);

/*
  srand(time(NULL));
  string graph;
  int matDim = 5;
  randLabels(vertex_labels,matDim);
  cout<<"VertexL size: " <<vertex_labels.size() <<endl;
  all_vertex_labels.push_back(vertex_labels);
  all_vertex_labels.push_back(vertex_labels);
  vertex_labels.clear();
  
  randMatrix(matrix,matDim);
  matrix.resize(matDim);
  for(int i = 0; i < matDim; i++){
	  matrix[i].resize(matDim);
  }
  randAlmostClique(matrix,matDim);
  randBinMatrix(matrix,matDim);
  matrices.push_back(matrix);
  matrix.clear();
  randMatrix(matrix,matDim);
  matrix = matrices[0];

  matrix.resize(matDim);
  for(int i = 0; i < matDim; i++){
	  matrix[i].resize(matDim);
  }
  
  matBinComplement(matrix,matrices[0]);
  emptyGraph(matrix,matDim);
  matRealComplement(matrix,matrices[0]);
  matrices.push_back(matrix);
  matrix.clear();
  */

  //read in the graph files
  string graph;
  vector<string> graph_names;
  if(graphsFile){
	  while(getline(graphsFile,graph)){
		graph_names.push_back(graph);
		readMatrix(matrix, vertex_labels,graph);
		//cout<<"numLabels: " <<vertex_labels.size() <<endl;
		matrices.push_back(matrix);
		all_vertex_labels.push_back(vertex_labels);
		matrix.clear();
		vertex_labels.clear();

	  }
  }
  else{
	  cerr<<"Error: Unable to open file with list of graphs\n";
	  exit(1);
  }
  if(matrices.size() == 1){
	  cerr<<"Only 1 graph file specified\nAt least 2 graphs are needed to calculate similarity\n";
	  exit(1);
  }
  //create a matrix for the scores
  simScores.resize(matrices.size());
  for(int i = 0; i < simScores.size(); i++){
	  simScores[i].resize(matrices.size());
  }
  
  //cout<<"Read matrix\n";
  //readKnown(argv[1], known_labels);   //read the known network from the file
 //Calculate similarity for all of the graphs
 for(int i = 0; i < matrices.size(); i++){
	 for(int j = i+1; j < matrices.size(); j++){
		 //run graph sim on these two matrices
		simScores[i][j] = graphSim(matrices[i],matrices[j],all_vertex_labels[i],all_vertex_labels[j],METHOD,phi);
	 }
 }
 
 //Flatten similarity matrix to sort
 int numPairs = atoi(argv[4]);
 vector<simScore> simScoresFlat;
 for(int i = 0; i < matrices.size(); i++){
	 for(int j = i+1; j < matrices.size(); j++){
		simScore temp;
		temp.i = i;
		temp.j = j;
		temp.similarity = simScores[i][j];
		simScoresFlat.push_back(temp);
	 }
 }
 sort(simScoresFlat.begin(),simScoresFlat.end());
 
	//skip past graphs with no common vertices
	int skip = 0;
	while(simScoresFlat[skip].similarity == -1.0){
		skip++;
		if(skip == simScoresFlat.size()){
			cout<<"No common vertices in any graphs\n";
			cout<<simScoresFlat.size() <<endl;
			return 0;
		}
	}
	//Outputs the specified number of pairs to stdout
	for(int i = 0; i < numPairs; i++){
		cout<<i+1 <<". Pair: " <<graph_names[simScoresFlat[skip+i].i] <<" " <<graph_names[simScoresFlat[skip+i].j] <<endl;
		if((skip+i) == simScoresFlat.size()){
			cout<<"No more network pairs with common vertices\n";
			return 0;
		}
	}



}
